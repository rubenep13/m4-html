<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <title>Taula del 6</title>
  </head>
  <body>
    <table border="2" align="center">
      <?php
      $n = 6;
      echo "<tr>";
      echo "<th>";
      echo "Multiplicador";
      echo "</th>";
      echo "<th>";
      echo "Resultat";
      echo "</th>";
      echo "</tr>";
      for ($x = 0; $x <= 10; $x++) {
        echo "<tr>";
        echo "<th>";
        echo $n . "x" . $x;
        echo "</th>";
          for ($y=1; $y <= 1; $y++) {
            $result = $x * $n;
            echo "<td>";
            echo $result;
            echo "</td>";
          }
        echo "</tr>";
      }
      ?>
    </table>
  </body>
  <footer>
   <a>Rubén Esteban</a>
  </footer> 
</html>
