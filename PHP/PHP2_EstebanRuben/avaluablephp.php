<!DOCTYPE html>
 <html lang="ca">
  <head>
   <meta charset="utf-8">
   <link rel="stylesheet" href="./avaluablephp.css">
   <title>Activitat Fitxers</title>
  </head>
  <body>
    <header>Activitat Fitxers</header>
    <div class="cos">
      <?php
	     function getDate(){
	      $date = date('d-m-Y H:i:s');
	      return $date;
	     }
	     $file = file($_FILES['fitxer']['tmp_name']);
	     $count = -1;
	     foreach ($file as $nlinia => $linia){
	        $count=$count+1;
	     }
	     $write=fopen('fitxers/fitxerResultant','w');
	     foreach ($file as $nlinia => $linia){
	        if($nlinia==$count){
	           fwrite($write, $linia."\n"."Text pujat el: ".getDate());
	        }else{
	           fwrite($write, $linia);
	        }
	     }
       fclose($write);
	     echo "<br>";
	     if($_FILES['fitxer']['error']>0){
	        echo "Hi han hagut errors durant la pujada";
	     }else{
	        echo "Arxiu pujat correctament";
	     }
	     echo "<br><br>";
	     echo "Contingut de l'arxiu:";
	     $upFile=file('fitxers/fitxerResultant');
	     echo "<br>";
	     foreach ($upFile as $nlinia2 => $linia2){
	        echo $linia2."<br>";
	     }
	     echo "<br><br>";
      ?>
    </div>
    <footer>Rubén Esteban</footer>
  </body>
</html>
